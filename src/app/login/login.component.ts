import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	email: string;
	router: Router;

	constructor(private route: Router) {
		this.router = route;
	}

	ngOnInit() {
		console.log("login: Init!");
	}

	_login(): void {
		if(this.email.trim() != ""){
			console.log(this.email);
			this.router.navigate(["/invoice/" + this.email])
		}
	}
}
